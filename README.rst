Django is a web framework for Python and in this file, we�re going to use Django to build a basic blog engine. Along the way you�ll pick up the smell of a typical Django workflow and get to sample a little taste of the �Pythonic Way�.

As we go through this tutorial, it�s really important that you read and type the code out by hand. Do not copy and paste code or you�ll miss critical learning experiences.

    Requirements
---------------------------
    Starting the project
    Starting the blog app
    Writing the blog models
    Creating the database
    Connecting the django admin to the blog app
    Writing the URLS, views and templates for the blog app
    Adding some style
    Suggestions for taking it further
